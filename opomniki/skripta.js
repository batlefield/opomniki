window.addEventListener('load', function() {
	//stran nalozena
		
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		var i;
		for (i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var naziv = opomnik.querySelector(".naziv_opomnika");
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
	        
	        if(cas <= 0) {
	        	alert("Opomnik!\n\nZadolžitev " + naziv.innerHTML + " je potekla!");
				
				opomnik.parentNode.removeChild(opomnik);
	        } else { 
	        	 casovnik.innerHTML = (cas - 1);	
	        }
		}
	}
	setInterval(posodobiOpomnike, 1000);
	
	document.getElementById("prijavniGumb").onclick = function(event) {
		var name = document.getElementById("uporabnisko_ime").value;
		
		var nameSpan = document.getElementById("uporabnik");
		nameSpan.innerHTML = name;
		
		var pokrivaloDiv = document.getElementsByClassName("pokrivalo")[0];
		pokrivaloDiv.style.display = "none";
	}
	
	document.getElementById("dodajGumb").onclick = function(event) {
		var naziv = document.getElementById("naziv_opomnika").value;
		var cas = document.getElementById("cas_opomnika").value;
		
		if(isNaN(cas) || cas == "") return;

		document.getElementById("naziv_opomnika").value = "";
		document.getElementById("cas_opomnika").value = "";
		
		var opomnikiList = document.getElementById("opomniki");
		
		opomnikiList.innerHTML = opomnikiList.innerHTML + 
			"<div class='opomnik senca rob'>" +
			"<div class='naziv_opomnika'>" + naziv + "</div>" + 
			"<div class='cas_opomnika'> Opomnik čez <span>" + cas + "</span> sekund.</div>" +
			"</div>";
	}
	
});